<?php

namespace App\Controller;

use App\Entity\Empresa;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EmpresaController extends AbstractController
{
    /**
     * @Route("/empreses", name="empreses")
     */
    public function index()
    {
        $empreses = $this->getDoctrine()->getRepository(Empresa::class)->getAllEmpreses();
        return $this->render('empresa/empresesSenseLogin.html.twig', ['empreses' => $empreses]);
    }

    /**
     * @Route("/empresa_detall/{id}", name="empresa_detall")
     */
    public function showDetail($id)
    {
        $empresa = $this->getDoctrine()->getRepository(Empresa::class)->getEmpresaDetail($id);
        return $this->render('empresa/detallEmpresa.html.twig', ['empresa' => $empresa[0]]);
    }
}
