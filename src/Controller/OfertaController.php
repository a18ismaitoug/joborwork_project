<?php

namespace App\Controller;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Oferta;

class OfertaController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index()
    {
        $ofertes = $this->getDoctrine()->getRepository(Oferta::class)->getAllOfertes();

        return $this->render('oferta/ofertesSenseLogin.html.twig', ['ofertes' => $ofertes]);

    }

    /**
     * @Route("/oferta_detail/{id}", name="ofera_detail")
     */
    public function showDetail($id)
    {

        $oferta = $this->getDoctrine()->getRepository(Oferta::class)->getOfertaDetail($id);

        //TODO
    }
}
