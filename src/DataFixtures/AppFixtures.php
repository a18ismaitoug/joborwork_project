<?php

namespace App\DataFixtures;

use App\Entity\Candidat;
use App\Entity\Empresa;
use App\Entity\Oferta;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        /*
        $empresa = new Empresa();
        $empresa->setCorreu('correu@iam.cat')
            ->setLogo('logo')
            ->setTipus('tipus');
        $manager->persist($empresa);
        */

        $max = 5;
        for ($i=0; $i<$max; $i++){

            //Load empresa
            $correu = 'empresa'.$i.'@iam.cat';
            $logo = 'logo_'.$i.'.png';
            $tipus = 'empresaProva-'.$i;
            $nom = 'empresa_'.$i;
            $empresa = new Empresa();
            $empresa->setCorreu($correu)
                    ->setLogo($logo)
                    ->setTipus($tipus)
                    ->setNom($nom);
            $manager->persist($empresa);

            //Load oferta
            $descripcio = 'descripcio prova '.$i;
            $titol = 'Titol Oferta_'.$i;
            $oferta = new Oferta();
            $oferta ->setDataPub(new \DateTime())
                    ->setDescripcio($descripcio)
                    ->setEmpresa($empresa)
                    ->setTitol($titol);
            $manager->persist($oferta);


            //Load Candidat
            $nom = 'CandidatTest_'.$i;
            $cognoms = 'Cognom';
            $estudis = 'estudis';
            $candidat = new Candidat();
            $candidat ->setNom($nom)
                        ->setCognom($cognoms)
                        ->setTelefon($i)
                        ->setOferta($oferta)
                        ->setEstudis($estudis);
            $manager->persist($candidat);

        }

        $max=4;
        for ($i=0; $i<$max; $i++){
            //Load Candidat without Oferta
            $nom = 'CandidatSenseOferta_'.$i;
            $cognoms = 'Cognom';
            $telefon = $i.$i.$i.$i;
            $candidat = new Candidat();
            $candidat ->setNom($nom)
                ->setCognom($cognoms)
                ->setTelefon($telefon);
            $manager->persist($candidat);
        }
        $manager->flush();
    }
}
